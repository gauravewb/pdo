
<?php
function resource_function() {

?>






<?php

$post_type = 'resources';
$taxonomies = get_object_taxonomies( array( 'post_type' => $post_type, 'posts_per_page' => 1 ) );
 
foreach( $taxonomies as $taxonomy ) :
 
    $terms = get_terms( $taxonomy );
 
    foreach( $terms as $term ) : 
    
    $term_link = get_term_link( $term ); 
    
    ?>
 
        <?php
        $args = array(
                'post_type' => $post_type,
                'posts_per_page' => 1,  //show all posts
                'tax_query' => array(
                    array(
                        'taxonomy' => $taxonomy,
                        'field' => 'slug',
                        'terms' => $term->slug,
                    )
                )
 
            );
        $posts = new WP_Query($args);
 
        if( $posts->have_posts() ): while( $posts->have_posts() ) : $posts->the_post(); ?>
 
            <div class="resources-items col-md-4">
                <a href="<?php the_field('page_links'); ?>">
                <div class="inner-post clearfix">
 
                    <div class="inner-img whitebox">
                    <?php if(has_post_thumbnail()) { ?>
                            <?php the_post_thumbnail(); ?>
                    <?php } ?>
                    </div>
                            <div class="resources-col">
				<div class="resources-title">
					<p><?php the_title(); ?></p>
				</div>
                                <div class="resources-content"><?php the_content(); ?></div>
                                </div>
                </div><!-- about-box -->
 </a>
 
            </div>
 
        <?php endwhile; endif; ?>
        </div>
        </div>
        </section>
 
    <?php endforeach;
 
endforeach; 
}

add_shortcode('resources-posts', 'resource_function');