  
<?php 
function resource_new_page()
{   ?>
<section id="blog-post">
<div class="container">
<div class="row">
<div class="col-md-8">
<?php
 global $paged;

$post_type = 'post';
$posts_per_page = '1';
$taxonomies = get_object_taxonomies( array( 'post_type' => $post_type, 'posts_per_page' => $posts_per_page ) );  
foreach( $taxonomies as $taxonomy ) :
    $terms = get_terms( $taxonomy );
    foreach ( $terms as $term ) : 
    $term_link = get_term_link( $term ); 
    
    ?>
     <?php  
        $args = array(
                'post_type' => $post_type,
                'posts_per_page' => $posts_per_page,  
                'tax_query' => array(
                    array(
                        'taxonomy' => $taxonomy,
                        'field' => 'slug',
                        'terms' => $term->slug,
                         )  
                                      )
                     );
             
        $posts = new WP_Query($args);
        if( $posts->have_posts() ): while( $posts->have_posts() ) : $posts->the_post(); ?>

<div class="blog-image">
 <?php if(has_post_thumbnail()) { ?>
 <?php the_post_thumbnail(); ?>
                    <?php } ?>
<div class="blog-overlay">
<span> <a href="<?php  echo $term_link  ?>"> <?php echo $term->name; ?></a> - <?php $post_date = get_the_date( 'F j, Y' ); echo $post_date;?> </span>
<h2> <a href="<?php  the_permalink();  ?>"><?php  the_title();  ?> </a></h2>
<p><?php echo wp_trim_words( get_the_content(), 30 ); ?></p>
<div class="blog-btn">
<a href="<?php  the_permalink();  ?>"> Read more <i class="fa fa-long-arrow-right"></i> </a>
</div>
</div>
</div> <!--blog-image-->
 <?php endwhile;
 wp_reset_postdata();

 endif; ?> 
 <?php endforeach; 
       endforeach;  ?>
</div> <!--col-md-8-->


<div class="col-md-4">
<div class="post-one-heading">
<div class="post-form">
<?php dynamic_sidebar( 'right-sidebar' ); ?>
</div>
<div class="blog-4"> 
 <?php dynamic_sidebar( 'right-sidebar-2' ); ?>
</div>            
<div class="first-section-lartes-news">
<h3>Recent Posts</h3>
<ul>
<?php

$sidebar = array( 'post_type' => 'post' , 'posts_per_page'=> 4 );
   $sidebarposts = new WP_Query($sidebar);
   if( $sidebarposts->have_posts() ): while( $sidebarposts->have_posts() ) : $sidebarposts->the_post(); 
   ?>
<li><a href="<?php the_permalink();  ?>"> <?php  the_title();  ?> </a> 
<p><?php $fname = get_the_author_meta('first_name');
$lname = get_the_author_meta('last_name');
$full_name = '';  
if( empty($fname)){
    $full_name = $lname;
} elseif( empty( $lname ))
{
    $full_name = $fname;
}
else {
    //both first name and last name are present
    $full_name = "{$fname} {$lname}";
}
echo $full_name; ?>  </a> <?php $post_date = get_the_date( 'F j, Y' ); echo $post_date;?>  </p></li>
<?php
endwhile;


endif;
?>
</ul>
</div>
</div> <!--col-md-4-->
</div>
</div>
</section>
<?php 
     } ?>
<?php
add_shortcode('resources-posts', 'resource_new_page');