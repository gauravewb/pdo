<?php

if(isset($_POST['insert'])) {
    
   include_once 'config.php';

    $fname = $_POST['fname'];
    $lname = $_POST['lname'];

    $pdoQuery = "INSERT INTO user (fname, lname) VALUES (:fname,:lname)";
    $pdoResult = $conn->prepare($pdoQuery);
    
    $pdoExec = $pdoResult->execute(array(":fname"=>$fname,":lname"=>$lname));
    
    if($pdoExec) {
        echo 'Data Inserted';
    } else {
        echo 'Data Not Inserted';
    }
}
?>