<?php	
/**	 * Template part for displaying posts.	 *	
 *  * @link    https://codex.wordpress.org/Template_Hierarchy	 *	
 *  * @package BigVenture	 
 */
?>
<div class="single-post">    
    <div class="container clearfix">  
        <div class="singletop-box">
        <figure class="post-thumbnail">            
            <?php //bigventure_post_thumbnail(); ?>  
            <?php the_post_thumbnail( 'full' ); ?>
            </figure><!-- .post-thumbnail -->                
        <header class="entry-header">            
            <div class="post-times"> 
            <?php  the_date(); ?>  </div>  
            <div class="post-right-boxs">
                <h2> <?php the_title(); ?> </h2>           
                    <?php if ( 'post' === get_post_type() ) : ?>                
            <div class="entry-metas">                    
                <?php   
                bigventure_meta_author('single',array('before' => esc_html__( 'by', 'bigventure' ) . ' ',));                    
                bigventure_meta_categories( 'single' );                    
                ?>       
            
            </div><!-- .entry-meta -->            
                <?php endif; ?>      
            </div>
        </header><!-- .entry-header -->  
        </div>
        <div class="entry-contents">            
            <?php the_content(); ?>                
                <?php			
                wp_link_pages( array(					
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bigventure' ),					
                    'after'  => '</div>',			
                    ) );		
                ?>	
        </div><!-- .entry-content -->			
            	
            <div class="single-post-shares">
                <h5>Share</h5>
                <?php bigventure_share_buttons( 'single' ); ?>
            </div>  
        
        
<div class="related-posts-secs">
    <div class="container clearfixs">
        <div class="related-headings">
                <h3>Related Posts</h3>
            </div>
        <div class="row">
            
     <?php

$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) );
if( $related ) foreach( $related as $post ) {
setup_postdata($post); ?>
 <div class="related-posts-items col-md-4"> 
        <div class="realted-posts-img">
            <?php bigventure_post_thumbnail( false ); ?> 
        </div>
     <div class="related-posts-content">          
            <div class="related-time"> <?php  the_date(); ?>  </div>  
            <div class="realted-content-box">
                <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>          
                    <?php if ( 'post' === get_post_type() ) : ?>                
            <div class="realted-cat">                    
                <?php   
                bigventure_meta_author('single',array('before' => esc_html__( 'by', 'bigventure' ) . ' ',));                    
                bigventure_meta_categories( 'single' );                    
                ?>       
            
            </div><!-- .entry-meta -->            
                <?php endif; ?>      
            </div>
        </div>
    </div>   
<?php }
wp_reset_postdata(); ?>
        </div>
</div>
</div>
       
<div class="single-post-comment">
<?php 
bigventure_meta_comments( 'single', array(						
                    'before' => '<i class="fa fa-comment"></i>',						
                    'after'  => esc_html__( 'comments ', 'bigventure' ),						
                    'zero'   => '0',						
                    'one'    => '1',						
                    'plural' => '%',				
                    ) );
?>
    </div>
    </div>
</div>

    
                
                