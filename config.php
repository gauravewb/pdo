<?php
$host = "127.0.0.1";
$db = "pdo";
$username = "root";
$password = "";


try {
    $conn = new PDO("mysql:host=$host;dbname=$db", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
    echo $conn . "<br>" . $e->getMessage();
    }
?>



<!--<//?php
class connection {
    private $server = "mysqli:host=localhost;dbname=pdo";
    private $user   = "root";
    private $pass   = "";
    private $options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,PDO::ATTR_DEFAUULT_FETCH_MODE => PDO:: FETCH_ASSOC,);
    protected $con;
            
         public function openConnection()
          {
            try
              {
                  $this->con = new PDO($this->server, $this->user,$this->pass,$this->options);
                  return $this->con;
            }
          catch (PDOException $e)
           {
             echo "there is some problem in connection: " . $e->getMessage(); 
           } 
          }
          public function closeConnection() {
              $this->con = null;
          }
     }
?>



